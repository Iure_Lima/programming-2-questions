# Padrões de Projeto
Explique qual é o design pattern que se encontra nos seguintes códigos (além de seu nome, explique seu problema e solução)

## Design pattern
Analisando o código fornecido nessa questão 2 do Lab. Fica evidente que estamos trabalhando com o design pattern Singleton Pattern. Cheguei a esse conclusão depois de analisar o código e perceber que nosso código esta mantendo uma classe global para a ConexaoDB. Quando percebi isso, cheguei a conclusão do nosso Design pattern.

## Problema e solução
Pelo que pude estar percebendo, esse código usando o Singleton Pattern esta resolvendo o problema de existir mais de uma instância da nossa classe ConexaoDB. A solução para evitar tal problema é simplesmente estar usando o Singleton para evitar que mais de uma instância de ConexaoDB possa existir ao mesmo tempo. Infelismente usar esse padrão mencionado acaba afetando os princípios do SOLID, por isso só podemos usar esse designer quando realmente necessário.
