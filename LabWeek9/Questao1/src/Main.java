import interfaces.Veiculos;
import navegacoes.NavegacaoRapida;
import navegacoes.NavegacaoSegura;
import navegacoes.Rotas;
import veiculos.Caminhao;
import veiculos.Carro;
import veiculos.Moto;

public class Main {
    public static void main(String[] args) {


        //Definindo veiculos
        Veiculos carro = new Carro();
        Veiculos caminhao = new Caminhao();

        // Definindo nossas fabricas
        FabricaVeiculo fabricaVeiculo1 = new FabricaVeiculo(carro);
        FabricaVeiculo fabricaVeiculo2 = new FabricaVeiculo(caminhao);

        //Definindo as rotas
        Rotas[] rotas = new Rotas[5];
        rotas[0] = new Rotas("Rota A", "alto", 100,"segura");
        rotas[1] = new Rotas("Rota B", "baixa", 80,"segura");
        rotas[2] = new Rotas("Rota C", "medio", 100,"medio");
        rotas[3] = new Rotas("Rota D", "baixo", 200,"perigosa");
        rotas[4] = new Rotas("Rota E", "baixo", 160,"perigosa");

        // definindo a estrategia de navegação de cada veiculo
        fabricaVeiculo1.definirEstrategiaNavegacao(new NavegacaoRapida(rotas));
        fabricaVeiculo2.definirEstrategiaNavegacao(new NavegacaoSegura(rotas));

        // mostrando as informações
        fabricaVeiculo1.mostrarInformacoes();
        System.out.println("--------------------------");
        fabricaVeiculo2.mostrarInformacoes();

    }
}