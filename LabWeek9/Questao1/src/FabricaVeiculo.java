import interfaces.EstrategiaNavegacao;
import interfaces.Veiculos;
import navegacoes.Rotas;

public class FabricaVeiculo {
    private Veiculos veiculo;
    private EstrategiaNavegacao estrategiaDeNavegacao;

    public FabricaVeiculo(Veiculos veiculo) {
        this.veiculo = veiculo;
    }

    public void definirEstrategiaNavegacao(EstrategiaNavegacao estrategiaDeNavegacao) {
        this.estrategiaDeNavegacao = estrategiaDeNavegacao;
    }

    public void mostrarInformacoes() {
        Rotas rotaEscolhida = estrategiaDeNavegacao.calcularCaminho(veiculo.maximaVelocidade());
        System.out.println("Tipo de veículo: " + veiculo.getClass().getSimpleName());
        System.out.println("A velocidade máxima é de "+veiculo.maximaVelocidade()+"km/h");
        System.out.println("A capacidade máxima é de "+veiculo.maximaCapacidade()+"kg");
        System.out.println("A rota escolhida foi: "+ (rotaEscolhida == null ? "Não achamos uma rota adequada": rotaEscolhida.getNomeRota()));
    }
}
