package navegacoes;

import interfaces.EstrategiaNavegacao;

public class NavegacaoRapida implements EstrategiaNavegacao {

    private Rotas[] rotas;

    public NavegacaoRapida(Rotas[] rotas) {
        this.rotas = rotas;
    }

    @Override
    public Rotas calcularCaminho(int velocidade) {
        System.out.println("Fazendo o calculo rota rápida...");
        for (int i = 0; i < rotas.length; i++) {
            if (rotas[i].getEstadoTrafego().equals("baixo") && rotas[i].getVelocidadeEstrada() >= velocidade){
                return rotas[i];
            }
        }
        return null;
    }
}

