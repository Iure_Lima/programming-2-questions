package navegacoes;

public class Rotas {
    private String estadoTrafego;
    private double velocidadeEstrada;

    private String estadoSegurancaEstradas;

    private String nomeRota;

    public Rotas(String nomeRota, String estadoTrafego, double velocidadeEstrada, String estadoEstradas) {
        this.estadoTrafego = estadoTrafego;
        this.velocidadeEstrada = velocidadeEstrada;
        this.estadoSegurancaEstradas = estadoEstradas;
        this.nomeRota = nomeRota;
    }

    public String getEstadoTrafego() {
        return estadoTrafego;
    }

    public double getVelocidadeEstrada() {
        return velocidadeEstrada;
    }

    public String getEstadoSegurancaEstradas() {
        return estadoSegurancaEstradas;
    }

    public String getNomeRota() {
        return nomeRota;
    }
}
