package navegacoes;

import interfaces.EstrategiaNavegacao;

public class NavegacaoSegura implements EstrategiaNavegacao {
    private Rotas[] rotas;

    public NavegacaoSegura(Rotas[] rotas) {
        this.rotas = rotas;
    }
    @Override
    public Rotas calcularCaminho(int velocidade) {
        System.out.println("Fazendo o calculo rota segura...");
        for (int i = 0; i < rotas.length; i++) {
            if (rotas[i].getEstadoSegurancaEstradas().equals("segura") && rotas[i].getEstadoTrafego().equals("medio")){
                return rotas[i];
            }
        }
        return null;
    }
}
