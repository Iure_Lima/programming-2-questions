package veiculos;

import interfaces.Veiculos;

public class Moto implements Veiculos {
    private  int cargaMaxima;
    private int velocidadeMaxima;

    public Moto() {
        this.cargaMaxima=90;
        this.velocidadeMaxima=190;
    }
    @Override
    public int maximaCapacidade() {
        return getCargaMaxima();
    }

    @Override
    public int maximaVelocidade() {
        return getVelocidadeMaxima();
    }

    public int getCargaMaxima() {
        return cargaMaxima;
    }

    public int getVelocidadeMaxima() {
        return velocidadeMaxima;
    }
}
