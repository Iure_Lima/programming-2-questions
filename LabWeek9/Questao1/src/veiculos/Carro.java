package veiculos;

import interfaces.Veiculos;

public class Carro implements Veiculos {
    private  int cargaMaxima;
    private int velocidadeMaxima;

    public Carro() {
        this.cargaMaxima=250;
        this.velocidadeMaxima=160;
    }

    @Override
    public int maximaCapacidade() {
        return getCargaMaxima();
    }

    @Override
    public int maximaVelocidade() {
        return getVelocidadeMaxima();
    }

    public int getCargaMaxima() {
        return cargaMaxima;
    }

    public int getVelocidadeMaxima() {
        return velocidadeMaxima;
    }
}
