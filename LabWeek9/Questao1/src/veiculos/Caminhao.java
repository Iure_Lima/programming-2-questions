package veiculos;

import interfaces.Veiculos;

public class Caminhao implements Veiculos {
    private  int cargaMaxima;
    private int velocidadeMaxima;

    public Caminhao() {
        this.cargaMaxima=2000;
        this.velocidadeMaxima=90;
    }

    @Override
    public int maximaCapacidade() {
        return getCargaMaxima();
    }

    @Override
    public int maximaVelocidade() {
        return getVelocidadeMaxima();
    }

    public int getCargaMaxima() {
        return cargaMaxima;
    }

    public int getVelocidadeMaxima() {
        return velocidadeMaxima;
    }
}
