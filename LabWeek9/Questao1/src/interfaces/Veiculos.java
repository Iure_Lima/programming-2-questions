package interfaces;

public interface Veiculos {
    int maximaCapacidade();
    int maximaVelocidade();
}
