package interfaces;
import navegacoes.Rotas;

public interface EstrategiaNavegacao {
    Rotas calcularCaminho(int velocidade);
}
