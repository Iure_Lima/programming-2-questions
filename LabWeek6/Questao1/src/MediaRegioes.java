import java.util.HashMap;
import java.util.Map;

public class MediaRegioes {
    private Map<Integer, String[]> dadosBrutos;
    private Map<String, Double> media = new HashMap<>();

    public MediaRegioes(Map<Integer, String[]> dadosBrutos) {
        this.dadosBrutos = dadosBrutos;
        calcularMedia();
    }

    private void calcularMedia(){
        String[] dadosAnos = dadosBrutos.get(0);
        int anos = dadosAnos.length -1;
        for (int i = 1; i < dadosBrutos.size() -1; i++) {
            Double media = (double) 0;
            String[] valores = dadosBrutos.get(i);
            for (int j = 1; j < valores.length; j++) {
                media += Double.parseDouble(valores[j]);
            }
            this.media.put(valores[0], media/anos);
        }
    }


    public void print(){
        System.out.println("Abaixo esta a tabela da região e a média do periodo de 2000 à 2014:");
        System.out.println();
        for (Map.Entry<String, Double> entry : media.entrySet()) {
            System.out.print(entry.getKey()+": ");
            System.out.println(entry.getValue());
        }
    }
}
