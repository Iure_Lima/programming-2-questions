import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Arquivo {
    private String arquivo = "src/pesquisa.csv";
    private Map<Integer, String[]> dados = new HashMap<>();

    public Arquivo() {
        readArquivo();

    }

    private void readArquivo(){
        BufferedReader reader = null;
        try{
            reader = new BufferedReader(new FileReader(arquivo));
            String linha;
            int cont = 0;
            while ((linha = reader.readLine()) != null){
                String[] dadosLinha = linha.split(";");
                dados.put(cont,dadosLinha);
                cont++;
            }

        }catch (IOException e){
            System.out.println("Erro ao tentar ler arquivo");
        }finally {
            try {
                if (reader != null){
                    reader.close();
                }
            }catch (IOException e){
                System.out.println("Erro ao fechar a conexão");
            }
        }
    }

    public Map<Integer, String[]> getDados() {
        return dados;
    }
}