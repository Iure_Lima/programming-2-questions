import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ReadArquivo {
    private String arquivo = "src/contatos.csv";
    private Map<String, String> dados = new HashMap<>();
    private String reset = "\u001B[0m";
    private String red = "\u001B[31m";

    private String green = "\u001B[32m";

    public ReadArquivo() {
        validandoArquivo();

    }
    private void validandoArquivo(){
        try{
            File file = new File(arquivo);
            if(!file.exists()){
                file.createNewFile();
                System.out.println();
                System.out.println(green+"Lista de contatos criada"+reset);
                System.out.println();
            }
        }catch (IOException e){
            System.out.println(red+"[ERRO] Falha ao criar o arquivo"+reset);
        }
        readArquivo();
    }

    private void readArquivo(){
        BufferedReader reader = null;
        try{
            reader = new BufferedReader(new FileReader(arquivo));
            String linha;
            while ((linha = reader.readLine()) != null){
                String[] dadosLinha = linha.split(",");
                dados.put(dadosLinha[0],dadosLinha[1]);
            }

        }catch (IOException e){
            System.out.println(red+"Erro ao tentar ler arquivo"+reset);
        }finally {
            try {
                if (reader != null){
                    reader.close();
                }
            }catch (IOException e){
                System.out.println(red+"Erro ao fechar a conexão"+reset);
            }
        }
    }

    public Map<String, String> getDados() {
        return dados;
    }
}