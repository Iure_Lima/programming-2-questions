import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

public class EditArquivo {
    private Map<String, Contato> contatos;
    private String arquivo = "src/contatos.csv";
    private String reset = "\u001B[0m";
    private String red = "\u001B[31m";

    public EditArquivo(Map<String, Contato> contatos) {
        this.contatos = contatos;
        updateContatos();
    }
    private void updateContatos(){
        try {
            FileWriter escrever = new FileWriter(arquivo,false);
            for (Map.Entry<String, Contato> entry: contatos.entrySet()) {
                escrever.write(entry.getValue().getNome()+","+entry.getValue().getNumero()+"\n");
            }
            escrever.close();
        }catch (IOException e){
            System.out.println();
            System.out.println(red+"Tivemso um problema ao sobreescrever o arquivo"+reset);
            System.out.println();
        }
    }
}
