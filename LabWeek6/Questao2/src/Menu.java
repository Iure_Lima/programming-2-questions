import java.util.Scanner;

public class Menu {
    private boolean menuOn = true;
    private int escolha = 0;
    private String reset = "\u001B[0m";
    private String red = "\u001B[31m";

    public void menuConfig(){
        escolha = 0;
        Scanner input = new Scanner(System.in);
        System.out.println();
        System.out.println("Você esta agora no menu:");
        System.out.println("1 - Adicionar um novo contato.");
        System.out.println("2 - Consultar um contato pelo nome.");
        System.out.println("3 - Remover um contato pelo nome.");
        System.out.println("4 - Listar todos os contatos");
        System.out.println("5 - Encerrar o programa.");
        try {
            System.out.print("Digite uma opção: ");
            escolha = input.nextInt();
            if (escolha > 5){
                escolha = 0;
                System.out.println();
                System.out.println(red+"Opção Invalida"+reset);
                System.out.println();
            }
        }catch (Exception e){
            System.out.println();
            System.out.println(red+"[Erro] Não conseguimos computar sua escolha"+reset);
            System.out.println();
        }
    }

    public boolean isMenuOn() {
        return menuOn;
    }

    public int getEscolha() {
        return escolha;
    }

    public void setMenuOn(boolean menuOn) {
        this.menuOn = menuOn;
    }
}
