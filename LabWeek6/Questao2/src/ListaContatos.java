import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;

public class ListaContatos {
    private Map<String, Contato> listaDeContatos = new HashMap<>();
    private Scanner input = new Scanner(System.in);
    private String reset = "\u001B[0m";
    private String red = "\u001B[31m";
    private String green = "\u001B[32m";

    public ListaContatos() {
        carregarContatos();
    }

    public void adicionarContato(){
        boolean sucesso = true;
        String nome;
        String numero;
        System.out.println();
        System.out.println("Adicionar contato");
        System.out.print("Digite o nome do contato: ");
        nome = input.nextLine();
        System.out.print("Digite o número do contato: ");
        numero = input.nextLine();

        boolean validacaoNumero = numero.matches("\\d+");
        boolean validacaoNome = nome.matches("[a-zA-Z0-9]+");

        if (!validacaoNumero) {
            sucesso = false;
        }

        if (!validacaoNome) {
            sucesso = false;
        }

        if (sucesso){
            if (!listaDeContatos.containsKey(nome)){
                Contato contato = new Contato(nome,numero);
                listaDeContatos.put(nome,contato);
                System.out.println();
                System.out.println(green+"Contato adicionado"+reset);
                System.out.println();
                salvarContatos();
            }else{
                System.out.println();
                System.out.println(red+"Esse contato já existe"+reset);
                System.out.println();
            }

        }else{
            System.out.println();
            System.out.println(red+"[ERRO] Contato não adicionado"+reset);
            System.out.println();
        }

    }

    public void consultarContato(){
        boolean sucesso = true;
        String nome;
        System.out.println();
        System.out.println("Buscar contato");
        System.out.print("Digite o nome do contato: ");
        nome = input.nextLine();

        boolean validacaoNome = nome.matches("[a-zA-Z0-9]+");

        if (!validacaoNome) {
            sucesso = false;
        }
        if (sucesso){
            if (listaDeContatos.containsKey(nome)){
                System.out.println(green+listaDeContatos.get(nome).getNumero()+ reset);
                System.out.println();
            }else{
                System.out.println();
                System.out.println(red+"Esse contato não existe"+reset);
                System.out.println();
            }
        }else{
            System.out.println();
            System.out.println(red+"[ERRO] Nome de contato invalido"+reset);
            System.out.println();
        }



    }

    public void removerContato(){
        boolean sucesso = true;
        String nome;
        System.out.println();
        System.out.println("Remover contato");
        System.out.print("Digite o nome do contato: ");
        nome = input.nextLine();

        boolean validacaoNome = nome.matches("[a-zA-Z0-9]+");

        if (!validacaoNome) {
            sucesso = false;
        }
        if (sucesso){
            if (listaDeContatos.containsKey(nome)){
                listaDeContatos.remove(nome);
                System.out.println();
                System.out.println(green+"Contato removido"+reset);
                System.out.println();
                salvarContatos();
            }else{
                System.out.println();
                System.out.println(red+"Esse contato não existe"+reset);
                System.out.println();
            }
        }else{
            System.out.println();
            System.out.println(red+"[ERRO] Nome de contato invalido"+reset);
            System.out.println();
        }
    }

    public void listarContatos(){
        System.out.println();
        System.out.println("Listando contatos");
        for (Map.Entry<String, Contato> entry: listaDeContatos.entrySet() ) {
            System.out.println(green+entry.getValue().getNome()+ "-" + entry.getValue().getNumero()+reset);
        }
        System.out.println();
    }

    private void carregarContatos(){
        ReadArquivo arquivo = new ReadArquivo();
        Map<String, String> dadosArquivos = arquivo.getDados();
        for (Map.Entry<String,String> entry: dadosArquivos.entrySet()) {
            Contato contato = new Contato(entry.getKey(), entry.getValue());
            listaDeContatos.put(entry.getKey(), contato);
        }
    }

    private void salvarContatos(){
        EditArquivo editArquivo = new EditArquivo(listaDeContatos);
    }

}
