public class Main {
    public static void main(String[] args) {
        String reset = "\u001B[0m";
        String green = "\u001B[32m";

        Menu menu = new Menu();
        ListaContatos listaContatos = new ListaContatos();

        do{
            menu.menuConfig();
            switch (menu.getEscolha()){
                case 1 -> listaContatos.adicionarContato();
                case 2 -> listaContatos.consultarContato();
                case 3 -> listaContatos.removerContato();
                case 4 -> listaContatos.listarContatos();
                case 5 -> {
                    System.out.println();
                    menu.setMenuOn(false);
                    System.out.println(green+"Programa finalizados"+reset);
                    System.out.println();
                }

            }
        }while (menu.isMenuOn());


    }
}