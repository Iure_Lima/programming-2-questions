
# Questão 1

## Descrição
Essa primeira questão tinha como objetivo criar um jogo de cartas onde o jogador e o computador receberiam aleatoriamente uma carta, depois as cartas eram comparadas e o ganhador era decidido.
## Resolução
Para a resolução dessa questão, eu criei um enum que definia o naipe do baralho, uma classe chamada carta, uma classe chamada baralho, e uma classe para definir quem foi o ganhador.
## Diagrama de Classe
![Diagrama de classe](https://github.com/iure06/Programming2-activitys/blob/main/LabWeek3/Questao1/src/images/diagramaDeClass.png)
## Execução
Execução 1

![Execução](https://github.com/iure06/Programming2-activitys/blob/main/LabWeek3/Questao1/src/images/execucao.png)

Execução 2

![Execução](https://github.com/iure06/Programming2-activitys/blob/main/LabWeek3/Questao1/src/images/execucao2.png)