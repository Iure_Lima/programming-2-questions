import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Random random = new Random();
        Deck deckPartida = new Deck();

        int jogadorEscolha = random.nextInt(10);
        int computadorEscolha = random.nextInt(10);
        Card cartaJogador = deckPartida.card(jogadorEscolha);
        Card cartaComputador = deckPartida.card(computadorEscolha);

        System.out.println("Estamos iniciando nosso Jogos");
        System.out.print("A carta do nosso jogador é: ");
        System.out.println(cartaJogador.getNaipe().getInicial()+cartaJogador.getAtributo());
        System.out.print("A carta do computador é: ");
        System.out.println(cartaComputador.getNaipe().getInicial()+cartaComputador.getAtributo());

        ResultParser resultParser = new ResultParser(cartaJogador,cartaComputador);

    }
}