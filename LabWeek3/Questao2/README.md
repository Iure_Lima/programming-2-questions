
# Questão 2

## Descrição
Nessa questão, temos que criar uma estrutura para gerenciar cartões de crédito, débito e pré-pago. Nesse desafio tínhamos que usar classes abstratas.
## Resolução
Para a resolução dessa questão, eu criei uma classe abstrata para cartão, e classes para cada cartão (débito, crédito, pré-pago) que herdam da classe cartão e por fim, foi criado uma classe conta. O main gerenciar o uso de cada um desses cartões.
## Diagrama de Classe

![Diagrama de classe](https://github.com/iure06/Programming2-activitys/blob/main/LabWeek3/Questao2/src/images/diagramaDeClasses.png)
## Execução

![Execução](https://github.com/iure06/Programming2-activitys/blob/main/LabWeek3/Questao2/src/images/execucao.png)