
# Questão 3

## Descrição
Nessa questão, temos que criar uma estrutura para gerenciar professores substitutos e titulares. Tínhamos que estar usando interfaces para resolver essa questão.
## Resolução
Criei uma interface para professor com seus devidos métodos, depois criei mais duas classes(Titular, Substituto) que implementavam a classe professor.
## Diagrama de Classe
![Diagrama de classe](https://github.com/iure06/Programming2-activitys/blob/main/LabWeek3/Questao3/src/images/diagramaDeClasse.png)
## Execução
![Execução](https://github.com/iure06/Programming2-activitys/blob/main/LabWeek3/Questao3/src/images/execucao.png)