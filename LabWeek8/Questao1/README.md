
# Documentação sobre os princípios KISS, DRY, YAGNI

## KISS
O princípio KISS é um acrônimo de "Keep It Simple, Stupid". Esse princípios sugere que as soluções de software sejam simples e fáceis de estarmos entendendo.

Na pasta Kiss dessa questão, temos um exemplo prático que representa esse princípio em ação.


## DRY
O princípo DRY, significa "Don't Repeat Yourself". Em outras palavras, esse princípio defende que cada funcionalidade de um software deve ter apenas uma única e clara representação no nosso código. Para isso, temos que estar evitando duplicar informações ou funcionalidade em nosso código.

Na pasta Dry dessa questão, temos um exemplo prático que representa esse princípio em ação.


## YAGNI
O princípio YAGNI, significa "You Ain't Gonna Need It". Em outra palavras, significa que não devemos estar adicionando funcionalidade ou complexidade ao nosso código a menos que isso seja absolutamente necessário. Não podemos estar implementando algo pensando que no futuro possa ser necessária, em vez disso, temos que estar adicionando as funcionalidades ao nosso código quando forem necessárias para o nosso projeto.

Na pasta Yagni dessa questão, temos um exemplo prático que representa esse princípio em ação.