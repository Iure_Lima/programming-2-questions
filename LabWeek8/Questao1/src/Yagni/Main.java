package Yagni;

public class Main {
    public static void main(String[] args) {
        Calculadora calculadora = new Calculadora();

        System.out.println(calculadora.somar(5, 5));
        System.out.println(calculadora.dividir(10, 2));
        System.out.println(calculadora.subtrair(50, 8));
        System.out.println(calculadora.multiplicar(2, 5));

    }
}
/*Podemos ver que nossa classe calculadora só possui as quatros operações básicas. Esse exemplo de código esta no princípio YAGNI pois,
* só estamos implementando os métodos que estaremos precisando e nada mais. Estamos evitando colocar métodos que não estão no escopo
* das necessidades do projeto para esse momento, evitando colocar métodos que só seram usados no futuro. Por esse motivo, é que acho que
* esse exemplo representa bem o conceito de YAGNI*/