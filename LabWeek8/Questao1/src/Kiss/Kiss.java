package Kiss;

/* Esse código abaixo representa o princípio KISS, pois a nossa função soma esta o mais simples possível
* se encaixando assim nesse princípio. Em vez de estarmos fazendo um método muito complexo para fazer a soma,
* estamos fazendo com que essa função sejá fácil de entender e que seja simples.*/

public class Kiss {
    public static void main(String[] args) {
        System.out.println(soma(4,5));
    }

    public static double soma(double a,double b){
        return a + b;
    }
}
