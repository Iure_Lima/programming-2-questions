package Dry;

public class Main {
    public static void main(String[] args) {
        Animal cachorro = new Cachorro("Apolo", 1);
        Animal gato = new Gato("Luck", 2);
        Animal papagaio = new Papagaio("Pipoca", 4);

        cachorro.comer();
        gato.comer();
        papagaio.comer();

    }
}

/*Como podemos ver nessa pasta, temos várias classe e o conjunto delas esta representando o princípio DRY. Todos os animais tem o método
* de comer, poderiamos estar impplementando esse método em cada animal, entretanto isso iria estár ferindo o princípio DRY. Para evitar
* isso, criamos uma classe abstrata que tem o método comer e estamos extendendo para as classes cachorro, gato e papagaio. Essa ação evitou
* estarmos duplicando nosso método de comer. Por esse motivo, creio que seja um bom exemplo do uso do DRY.*/
