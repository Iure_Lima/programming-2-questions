import ClassesTarefas.RepositorioTarefasMemoria;
import ClassesTarefas.Tarefa;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //Intancias
        RepositorioTarefasMemoria repositorioTarefas = new RepositorioTarefasMemoria();
        Menu menu = new Menu();
        Scanner input = new Scanner(System.in);

        //O loop abaixo serve para deixar o programa sempre rodando e implementando as escolhas para facilitar os testes

        do{
            menu.printMenu();
            switch (menu.getEscolha()){
                //Nessa opção estamos adicionando as tarefas
                case 1 ->{
                    String titulo, descricao;

                    System.out.println("Adicionar Tarefa");
                    System.out.print("Digite um titulo: ");
                    titulo = input.nextLine();

                    System.out.print("Digite uma descrição: ");
                    descricao = input.nextLine();

                    repositorioTarefas.adicionar(new Tarefa(titulo,descricao));
                    System.out.println("\nTarefa adicionada com sucesso\n");
                }
                //Nessa opção estamos atualizando as tarefas
                case 2 ->{
                    System.out.println("Atualizar Tarefa");
                    System.out.print("Digite o título da tarefa: ");
                    repositorioTarefas.atualizar(input.nextLine());

                }
                //Nessa opção estamos listando as tarefas
                case 3 -> {
                    System.out.println("\nLista de tarefas:\n");
                    for (Tarefa t: repositorioTarefas.buscarTarefas()) {
                        System.out.println(t.toString());
                        System.out.println();
                    }
                }
                //Nessa opção fechamos o nosso programa
                case 4 -> System.out.println("\nFechando o programa\n");
                default -> System.out.println("\nOpção Invalida\n");
            }

        }while (menu.getEscolha() != 4);

        input.close();
    }
}