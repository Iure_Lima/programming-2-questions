import java.util.Scanner;

public class Menu {
    private int escolha;

    public void printMenu(){
        System.out.println();
        System.out.println("Escolha uma das opções abaixo:");
        System.out.println("1 - Adicionar Tarefa");
        System.out.println("2 - Atualizar Tarefa");
        System.out.println("3 - Listar Tarefas");
        System.out.println("4 - Fechar Programa");
        System.out.print("Sua escoha: ");
        setEscolha();
    }

    private void setEscolha(){
        Scanner input = new Scanner(System.in);
        try {
            escolha = input.nextInt();
            System.out.println();
        }catch (Exception e){
            System.out.println("\nERRO ao computar sua escolha\n");
        }
    }

    public int getEscolha() {
        return escolha;
    }
}
