package ClassesTarefas;

import java.util.ArrayList;
import java.util.List;

//Classe que representa o Dependency Inversion Principle
public class RepositorioTarefasMemoria implements RepositorioTarefa{
    private List<Tarefa> tarefas = new ArrayList<>();

    @Override
    public void adicionar(Tarefa tarefa) {
        tarefas.add(tarefa);
    }

    @Override
    public void atualizar(String titulo) {
        for (Tarefa t:tarefas) {
            if(t.getTitulo().equals(titulo)){
                t.setConcluida(true);
                System.out.println("\nTarefa atualizada\n");
                return;
            }
        }
        System.out.println("\nTarefa não encontrada\n");

    }

    @Override
    public Tarefa[] buscarTarefas() {
        return tarefas.toArray(new Tarefa[0]);
    }
}
