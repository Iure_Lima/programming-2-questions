package ClassesTarefas;

//Interface que representa o Open/closed principle.

interface RepositorioTarefa {

    void adicionar(Tarefa tarefa);

    void atualizar(String titulo);

    Tarefa[] buscarTarefas();
}
