package animais;


import especies.Mamifero;

public class Morcego extends Mamifero {
    private String name = "Morcego";

    public void voar(){
        System.out.println("Pode voar");
    }

    public String getName() {
        return name;
    }

}
