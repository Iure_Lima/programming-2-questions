package animais;
import especies.Passaro;


public class Pato extends Passaro {
    private String name = "Pato";

    public void andar(){
        System.out.println("Pode andar");
    }

    public void nadar(){
        System.out.println("Pode nadar");
    }

    public String getName() {
        return name;
    }


}
