import orquestra.Instrumento;
import orquestra.Orquestra;
import orquestra.instrumentos.corda.Guitarra;
import orquestra.instrumentos.percussao.Tambor;
import orquestra.instrumentos.sopro.Trompete;

public class Main {
    public static void main(String[] args) {
        Instrumento guitarra = new Guitarra();
        Instrumento tambor = new Tambor();
        Instrumento trompete = new Trompete();

        Orquestra orquestra = new Orquestra();

        orquestra.add(guitarra);
        orquestra.add(tambor);
        orquestra.add(trompete);
        orquestra.play();
        orquestra.play(1);
    }
}