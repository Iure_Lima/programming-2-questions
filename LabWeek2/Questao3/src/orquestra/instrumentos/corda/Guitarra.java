package orquestra.instrumentos.corda;

import orquestra.Instrumento;

public class Guitarra extends Instrumento {
    @Override
    public void play() {
        toStriing();
    }

    @Override
    public void toStriing() {
        System.out.println("Guitarra: soa");
    }
}
