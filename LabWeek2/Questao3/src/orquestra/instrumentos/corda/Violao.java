package orquestra.instrumentos.corda;

import orquestra.Instrumento;

public class Violao extends Instrumento {
    @Override
    public void play() {
        toStriing();
    }

    @Override
    public void toStriing() {
        System.out.println("Violão: soa");
    }
}
