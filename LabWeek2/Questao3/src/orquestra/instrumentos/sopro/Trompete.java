package orquestra.instrumentos.sopro;

import orquestra.Instrumento;

public class Trompete extends Instrumento {
    @Override
    public void play() {
        toStriing();
    }

    @Override
    public void toStriing() {
        System.out.println("Trompete: apita");
    }
}
