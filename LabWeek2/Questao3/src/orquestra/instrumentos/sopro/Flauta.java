package orquestra.instrumentos.sopro;

import orquestra.Instrumento;

public class Flauta extends Instrumento {
    @Override
    public void play() {
        toStriing();
    }

    @Override
    public void toStriing() {
        System.out.println("Flauta: apita");
    }
}
