import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        MySet mySet = new MySet();
        Scanner input = new Scanner(System.in);

        while (true){
            int number;
            System.out.print("Digite um número: ");
            try {
                number = input.nextInt();
                if (number < 0){
                    break;
                }else{
                    mySet.add(number);
                }
                mySet.printArray();
            }catch (InputMismatchException e){
                System.out.println("Dado invalido!");
                input.nextLine();
            }


        }

    }
}