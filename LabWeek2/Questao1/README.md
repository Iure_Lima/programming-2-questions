
# Questão 1 Tratamento de exceções

Nessa primeira questão do lab, tinhamos que criar uma classe que permitisse adicionar números positivos em uma lista de inteiro usando métodos dessa classe. Tinhamos que ter tratamento de erros para caso o número já estivesse na lista, caso o número de elementos excedesse 100 elementos e se um elemento fosse nulo.


## Código do Diagrama classe

Abaixo esta o código do diagrama de classe feito no Mermaid.

    classDiagram

    class Myset{
        int[] listInt
        void Myset()
        void add(int number)
        void printArray()
        bool validacao(int number)

    }

## Imagem do Diagrama de classe
![Diagrama](https://github.com/iure06/Programming2-activitys/blob/main/LabWeek2/Questao1/src/images/diagramaDeClass.png)

## Execução
Abaixo esta a imagem que mostra o resultado da execução do código dessa questão.
![Execução](https://github.com/iure06/Programming2-activitys/blob/main/LabWeek2/Questao1/src/images/execucao.png)