package gerenciadores;

import java.util.ArrayList;
import java.util.LinkedList;

public class GerenciadorLinkedList <T> implements Gerenciador{
    private LinkedList<T> linkedList = new LinkedList<>();
    private LinkedList<T> linkedListRepetido = new LinkedList<>();

    public void filter(){

        for (int i = 0; i < linkedList.size(); i++) {
            for (int j = 0; j < linkedList.size(); j++) {
                if (linkedList.get(i) == linkedList.get(j) && i != j){
                    linkedListRepetido.add(linkedList.get(j));
                    linkedList.remove(j);

                }
            }
        }
        print();

    }
    public void addAll(T[] values){
        for (int i = 0; i < values.length; i++) {
            linkedList.add(values[i]);
        }
    }
    private void print(){
        System.out.println("Usando o LinkedList");
        System.out.print("Resultado: ");
        System.out.println(linkedList);
        System.out.print("Filtro: ");
        System.out.println(linkedListRepetido);
        System.out.println();
    }

}
