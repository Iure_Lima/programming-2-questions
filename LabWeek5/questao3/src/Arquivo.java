import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Arquivo {
    private Queue<String> pilha = new PriorityQueue<>();
    private String arquivo;

    public Arquivo() {
        Scanner input = new Scanner(System.in);

        System.out.print("Digite o nome do arquivo: ");
        String nomeArquivo = input.next();
        arquivo = "src/"+nomeArquivo+".txt";
        readArquivo();

    }

    private void readArquivo(){
        BufferedReader reader = null;
        try{
            reader = new BufferedReader(new FileReader(arquivo));
            String linha;
            while ((linha = reader.readLine()) != null){
                char[] proteinas = linha.toCharArray();
                for (int i = 0; i < proteinas.length; i++) {
                    pilha.offer(String.valueOf(proteinas[i]));
                }
            }

        }catch (IOException e){
            System.out.println("Erro ao tentar ler arquivo");
        }finally {
            try {
                if (reader != null){
                    reader.close();
                }
            }catch (IOException e){
                System.out.println("Erro ao fechar a conexão");
            }
        }
    }

    public Queue<String> getPilha() {
        return pilha;
    }
}
