import java.util.Queue;

public class Main {
    public static void main(String[] args) {
        Arquivo arquivo = new Arquivo();
        Queue<String> proteinas = arquivo.getPilha();

        if(!proteinas.isEmpty()){
            Proteina proteina = new Proteina();
            double massa = proteina.encontrarMassa(proteinas);
            System.out.println("A massa da Proteina é: "+massa);
        }

    }
}