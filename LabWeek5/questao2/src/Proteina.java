import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class Proteina {
    private Map<String, Float> listaProteinas = new HashMap<>();

    public Proteina() {
        preencherLista();
    }

    public double encontrarMassa(Stack<String> values){
        System.out.println("============== Proteina ===============");
        double massa = 0;
        int i = 1;
        while (!values.empty()){
            if (listaProteinas.containsKey(values.peek())){
                System.out.print(values.peek()+" ");
                massa += listaProteinas.get(values.pop());
            }
            if (i % 20 == 0){
                System.out.println();
            }
            i++;
        }
        System.out.println();
        System.out.println("=======================================");
        return massa;

    }

    private void preencherLista(){
        BufferedReader reader = null;
        try{
            reader = new BufferedReader(new FileReader("src/tabelaProteinas.txt"));
            String linha;
            while ((linha = reader.readLine()) != null){
                String[]  dados = linha.split(" ");
                String letra = dados[0];
                Float peso =  Float.parseFloat(dados[1]);
                listaProteinas.put(letra, peso);
            }

        }catch (IOException e){
            e.printStackTrace();
        }finally {
            try {
                if (reader != null){
                    reader.close();
                }
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }


}
