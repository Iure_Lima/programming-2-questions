import java.util.Stack;

public class Main {
    public static void main(String[] args) {
        Arquivo arquivo = new Arquivo();
        Stack<String> proteinas = arquivo.getPilha();

        if(!proteinas.empty()){
            Proteina proteina = new Proteina();
            double massa = proteina.encontrarMassa(proteinas);
            System.out.println("A massa da Proteina é: "+massa);
        }

    }
}