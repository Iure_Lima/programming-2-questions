import Arquivo.EditArquivo;
import Arquivo.ReadArquivo;
import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        Menu menu = new Menu();

        String nomeDoArquivo ;


        try{
            do {
                System.out.print("Digite o apenas o nome do arquivo: ");
                nomeDoArquivo = input.next();
                if (nomeDoArquivo.matches("[a-zA-Z0-9]+")) break; else System.out.printf("%nNome de arquivo invalido%n");
            }while (true);
            input.nextLine();

            ReadArquivo readArquivo = new ReadArquivo(nomeDoArquivo);
            EditArquivo editArquivo = new EditArquivo(nomeDoArquivo);

            ProcessadorCovid processador = new ProcessadorCovid(readArquivo.readArquivo());

            do {
                menu.print();
                switch (menu.getEscolha()){
                    case 1 -> {
                        System.out.print("\n"+"-----Adicionando registro-----"+"\n");
                        processador.adicionandoNovoRegistro(editArquivo);
                    }
                    case 2 -> {
                        System.out.print("\n"+"-----Consultar Taxa de Cresscimento-----"+"\n");
                        System.out.print("Digite o nome do Bairro: ");
                        System.out.println(processador.taxaCrescimento(input.nextLine().toLowerCase()));
                    }
                    case 3 -> {
                        System.out.println("\n"+"-----Consultar Taxa de Letalidade-----"+"\n");
                        System.out.print("Digite o nome do Bairro: ");
                        System.out.println(processador.taxaLetalidade(input.nextLine().toLowerCase()));
                    }
                    case 4 -> {
                        System.out.println("\n"+"-----Lista de bairros mais impactados-----"+"\n");
                        processador.listaBairrosImpactados();
                    }
                    case 5 -> System.out.printf("%nFechando Progama%n");
                    default -> System.out.printf("%nOpção Invalida!%n");
                }

            }while (menu.getEscolha() != 5);
        }catch (Exception e){
            System.out.print("%nERRO, esse nome dea arquivo é invalido");
        }
    }
}