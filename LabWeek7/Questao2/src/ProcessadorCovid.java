import Arquivo.EditArquivo;
import ModeloBairro.BairroCovid;
import java.util.*;
import java.util.stream.Stream;

public class ProcessadorCovid {
    private BairroCovid[] bairros;
    private String reset = "\u001B[0m";
    private String red = "\u001B[31m";

    private String green = "\u001B[32m";

    public ProcessadorCovid(BairroCovid[] bairros) {
        this.bairros = bairros;
    }


    //Adiciona um novo registro
    public void adicionandoNovoRegistro(EditArquivo arquivo){
        Scanner input = new Scanner(System.in);

        String nomeBairro, data;
        int obitos,casosConfirmados;

        System.out.print("Digite o nome do bairro: ");
        nomeBairro = input.next();

        System.out.print("Digite o número de casos: ");
        casosConfirmados = input.nextInt();

        System.out.print("Digite o número de óbitos: ");
        obitos = input.nextInt();

        System.out.print("Digite data de analise [AAAA-MM-DD]: ");
        data = input.next();

        //Criamos um novo bairro com os dados fornecidos
        BairroCovid novoBairro = new BairroCovid(nomeBairro,casosConfirmados,obitos,data);

        getNovaLista(novoBairro);

        //Salvando arquivo
        arquivo.setBairros(bairros);

        System.out.printf(green+"%nRegistro salvo %n"+reset);


    }

    private void getNovaLista(BairroCovid novoBairro){
        //Aqui estamos fazendo uma nova lista de bairros com o novo bairro adicionado
        BairroCovid[] newBairros = new BairroCovid[bairros.length+1];
        System.arraycopy(bairros, 0, newBairros, 0,bairros.length);
        newBairros[newBairros.length-1] = novoBairro;
        bairros = newBairros;
    }

    //Calcula a taxa de crescimento de casos e retorna a porcentagem de todo o periodo
    public String taxaCrescimento(String nomeBairro) {
        Stream<BairroCovid> bairroCovidStream = Arrays.stream(totalRegistrosBairro(nomeBairro))
                .sorted(Comparator.comparing(BairroCovid::getData));

        Stream<BairroCovid> bairroCovidStream2 = Arrays.stream(totalRegistrosBairro(nomeBairro))
                .sorted(Comparator.comparing(BairroCovid::getData));

        double taxaCrescimento;
        int valorInicial = bairroCovidStream.map(BairroCovid::getCasosConfirmados).findFirst().orElse(0);
        int valorFinal = bairroCovidStream2.map(BairroCovid::getCasosConfirmados).reduce((primeiro, segundo) -> segundo).orElse(0);

        taxaCrescimento = (double) (valorFinal - valorInicial) / valorInicial;


        if(taxaCrescimento == 0) return red+"Bairro não encontrado"+reset;
        return "A taxa de letalidade de "+nomeBairro+" é de "+green+(taxaCrescimento*100)+reset+"%\n";
    }

    private BairroCovid[] totalRegistrosBairro(String nomeBairro){
        ArrayList<BairroCovid> bairrosCovid = new ArrayList<>();

        for (BairroCovid bairro:bairros) {
            if (bairro.getNomeBairro().equals(nomeBairro)){
                bairrosCovid.add(bairro);
            }
        }
        return bairrosCovid.toArray(new BairroCovid[0]);
    }

    //Calcula a taxa de letalidade de um bairro e retorna o valor em porcentagem
    public String taxaLetalidade(String nomeBairro){
        Stream<BairroCovid> bairroCovidStream = Arrays.stream(totalRegistrosBairro(nomeBairro))
                .sorted(Comparator.comparing(BairroCovid::getData).reversed());

        Optional<BairroCovid> primeiroBairroCovid = bairroCovidStream.findFirst();

        double taxaLetalidade = 0;

        if (primeiroBairroCovid.isPresent()){
            BairroCovid primeiroBairro = primeiroBairroCovid.get();
            System.out.println(primeiroBairro.getObitos() + primeiroBairro.getCasosConfirmados());
            taxaLetalidade = (double) primeiroBairro.getObitos() / primeiroBairro.getCasosConfirmados() ;
        }

        if(taxaLetalidade == 0) return red+"Bairro não encontrado"+reset;
        return "A taxa de letalidade de "+nomeBairro+" é de "+green+(taxaLetalidade*100)+reset+"%\n";
    }

    //Lista de bairros mais impactados
    public void listaBairrosImpactados(){
        Stream<BairroCovid> bairrosStream = Arrays.stream(bairros);
        bairrosStream.sorted(Comparator.comparing(BairroCovid::getCasosConfirmados).reversed()).limit(5).forEach(System.out::println);
    }
}
