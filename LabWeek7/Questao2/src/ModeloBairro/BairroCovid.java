package ModeloBairro;

public class BairroCovid {
    private String nomeBairro, data;
    private int casosConfirmados, obitos;

    public BairroCovid(String nomeBairro,  int casosConfirmados, int obitos,String data) {
        this.nomeBairro = nomeBairro;
        this.data = data;
        this.casosConfirmados = casosConfirmados;
        this.obitos = obitos;
    }

    public String getNomeBairro() {
        return nomeBairro;
    }

    public String getData() {
        return data;
    }

    public int getCasosConfirmados() {
        return casosConfirmados;
    }

    public int getObitos() {
        return obitos;
    }

    public String toString(){
        return "Nome: " + nomeBairro +"\n"+
                "Casos Confirmados: " + casosConfirmados +"\n"+
                "Obitos: " + obitos +"\n"+
                "Data de verificação: " + data + "\n" ;
    }
}
