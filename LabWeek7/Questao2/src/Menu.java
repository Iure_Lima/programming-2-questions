import java.util.Scanner;

public class Menu {
    private int escolha;

    public void print(){
        System.out.printf("%nEsolha uma das opções abaixo:%n");
        System.out.println("1 - Adicionar um novo registro");
        System.out.println("2 - Calcular taxa de crescimento de casos");
        System.out.println("3 - Calcular taxa de letalidade");
        System.out.println("4 - Exibir bairros mais impactados");
        System.out.println("5 - Fechar programa");


        setEscolha();
    }
    private void setEscolha(){
        Scanner input = new Scanner(System.in);

        System.out.print("Sua escolha: ");
        try{
            escolha = input.nextInt();
        }catch (Exception e){
            System.out.printf("%n [ERRO] Tivemos um erro ao computar sua escolha %n");
        }

    }

    public int getEscolha() {
        return escolha;
    }
}
