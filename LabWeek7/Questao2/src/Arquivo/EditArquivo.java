package Arquivo;

import ModeloBairro.BairroCovid;

import java.io.FileWriter;
import java.io.IOException;

public class EditArquivo {
    private BairroCovid[] bairros;
    private String nomeArquivo = "src/";

    public EditArquivo(String nomeArquivo) {
        this.nomeArquivo += nomeArquivo + ".csv";
    }

    private void salvaArquivo(){
        //Cores para o terminal quando tivermos um erro
        String reset = "\u001B[0m";
        String red = "\u001B[31m";

        try{
            FileWriter escreverArquivo = new FileWriter(nomeArquivo, false);
            for (BairroCovid bairro: bairros) {
                escreverArquivo.write(bairro.getNomeBairro()+","+bairro.getCasosConfirmados()+","+bairro.getObitos()+","+bairro.getData()+"\n");
            }
            escreverArquivo.close();
        }catch (IOException e){
            System.out.printf(red+"%n Tivemos problema ao salvar seu registro%n"+reset);
        }
    }

    public void setBairros(BairroCovid[] bairros) {
        this.bairros = bairros;
        salvaArquivo();
    }
}
