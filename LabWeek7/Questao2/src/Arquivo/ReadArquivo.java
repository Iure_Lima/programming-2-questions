package Arquivo;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import ModeloBairro.BairroCovid;
public class ReadArquivo {
    private String arquivo = "src/";
    private String reset = "\u001B[0m";
    private String red = "\u001B[31m";

    private String green = "\u001B[32m";

    public ReadArquivo(String arquivo) {
        this.arquivo += arquivo + ".csv";
        validandoArquivo();
    }

    private void validandoArquivo(){
        try{
            File file = new File(arquivo);
            if(!file.exists()){
                file.createNewFile();
                System.out.printf(green+"%nArquivo Criado%n"+reset);
            }
        }catch (IOException e){
            System.out.println(red+"[ERRO] Falha ao criar o arquivo"+reset);
        }
    }

    public BairroCovid[] readArquivo(){
        try {
            FileReader fileReader = new FileReader(arquivo);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String linha;

            ArrayList<BairroCovid> bairros = new ArrayList<>();
            while ((linha = bufferedReader.readLine()) != null) {
                String[] valores = linha.split(",");
                bairros.add(new BairroCovid(valores[0].toLowerCase(), Integer.parseInt(valores[1]) , Integer.parseInt(valores[2]) , valores[3]));
            }
            bufferedReader.close();

            return bairros.toArray(new BairroCovid[0]);
        } catch (java.io.IOException err) {
            throw new RuntimeException("Arquivo inválido!");
        }
    }

}