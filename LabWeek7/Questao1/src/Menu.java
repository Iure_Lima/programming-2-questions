import java.util.Scanner;

public class Menu {
    private int escolha;

    public void printMenu(){
        System.out.println();
        System.out.println("Digite uma das opções abaixo:");
        System.out.println("1 - Simples");
        System.out.println("2 - Cinco Distâncias ímpares");
        System.out.println("3 - Ordem crescente");
        System.out.println("4 - Ordem decrescente (mostre apenas distâncias)");
        System.out.println("5 - Sair");
        escolhaOpcao();
    }

    private void escolhaOpcao(){
        Scanner input = new Scanner(System.in);
        System.out.print("Digite aqui: ");
        try{
            escolha = input.nextInt();
            System.out.println();
        }catch (Exception e){
            System.out.println("Erro ao computar a sua escolha.");
        }
    }

    public int getEscolha() {
        return escolha;
    }
}
