import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

public class Utils {
    private String nomeArquivo;

    public Utils(String nomeArquivo) {
        this.nomeArquivo = nomeArquivo;
    }

    public Distancia[] readArquivo(){
        try {
            FileReader fileReader = new FileReader(nomeArquivo);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String linha;
            ArrayList<Distancia> distancias = new ArrayList<>();

            while ((linha = bufferedReader.readLine()) != null) {
                String[] valores = linha.split(",");
                distancias.add(new Distancia(valores[0], valores[1], Double.parseDouble(valores[2])));
            }

            bufferedReader.close();

            return distancias.toArray(new Distancia[0]);
        } catch (java.io.IOException err) {
            throw new RuntimeException("Arquivo inválido!");
        }
    }
}

