import java.util.Objects;

public class Distancia implements Comparable<Double>{
    private String origem, destino;
    private Double dist;

    public Distancia(String origem, String destino, double dist) {
        this.origem = origem;
        this.destino = destino;
        this.dist = dist;
    }

    @Override
    public int compareTo(Double valor) {
        return dist.compareTo(valor);
    }

    public int hashCode(){
        return Objects.hash(origem, destino, dist);
    }

    public String toString(){
        return origem + " => " + destino + ": " + dist;
    }

    public boolean equals(Object objeto){
        if (objeto == null || getClass() != objeto.getClass()){
            return false;
        }
        Distancia distancia = (Distancia) objeto;
        return  Objects.equals(origem, distancia.origem) && Objects.equals(destino, distancia.destino) && Objects.equals(dist, distancia.dist);
    }

    public Double getDist() {
        return dist;
    }
}
