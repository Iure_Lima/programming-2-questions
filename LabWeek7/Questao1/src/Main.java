import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        Distancia[] distanciasArquivo = new Utils("src/cidades.csv").readArquivo();
        Menu menu = new Menu();

        do{
            menu.printMenu();
            Stream<Distancia> distancias = Arrays.stream(distanciasArquivo);
            switch (menu.getEscolha()){
                case 1 -> distancias.forEach(System.out::println);
                case 2 -> distancias.filter((elemento) -> elemento.getDist() % 2 == 1).limit(5).forEach(System.out::println);
                case 3 -> distancias.sorted(Comparator.comparing(Distancia::getDist)).forEach(System.out::println);
                case 4 -> distancias.sorted(Comparator.comparing(Distancia::getDist).reversed()).forEach(distancia -> System.out.println(distancia.getDist()));
                case 5 -> System.out.println("Fechando o Programa");
                default -> System.out.println("Opção Invalida!");
            }

        }while (menu.getEscolha() != 5);
    }
}