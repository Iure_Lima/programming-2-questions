
# OOP Combinada com Matriz Simples

Esse projeto aborda a resolução do problema da terceira questão do Lab Week da semana 1 do curso de Programming da Jala University.





## Funcionalidades

Esse código tem 5 funcionalidade, são elas:

- Adicionar aluno a aniversidade;
- Adicionar curso ao aluno;
- Adicionar nota ao aluno;
- Lista o nome dos alunos de um determinado curso;
- Listar todos os alunos da universidade;

Abaixo está um seleção de imagens que mostra o uso de todas essas funcionabilidade comentadas acima.

### Adicionar aluno a universidade
![Image01](https://github.com/iure06/Programming2-activitys/blob/main/LabWeek1/Questao3/src/images/AlunoAdd.png)

### Adicionar curso ao aluno
![Image02](https://github.com/iure06/Programming2-activitys/blob/main/LabWeek1/Questao3/src/images/CursoAddAluno.png)

### Adicionar nota ao aluno
![Image03](https://github.com/iure06/Programming2-activitys/blob/main/LabWeek1/Questao3/src/images/modificandoNota.png)

### Lista o nome dos alunos de um determinado curso.
![Image04.1](https://github.com/iure06/Programming2-activitys/blob/main/LabWeek1/Questao3/src/images/buscarAlunoCurso1.png)


![Image04.2](https://github.com/iure06/Programming2-activitys/blob/main/LabWeek1/Questao3/src/images/buscaAlunoCurso2.png)

### Listar todos os alunos da universidade
![Image05](https://github.com/iure06/Programming2-activitys/blob/main/LabWeek1/Questao3/src/images/listarAlunos.png)
## Código
Abaixo tem todo o código de cada classe usada na resolução desse problema.

#### Class main
    import java.util.Scanner;

    public class Main {
        static Scanner teclado = new Scanner(System.in);
        public static void main(String[] args) {
        Universidade universidade = new Universidade();
        int opcao;

        while (true){
            menu();

            System.out.print("Qual a sua escolha: ");
            opcao = teclado.nextInt();
            do{
                if (opcao > 0 && opcao <= 5){
                    break;
                }else {
                    System.out.println("Opção invalida! Tente novamente.");
                    System.out.print("Qual a sua escolha: ");
                    opcao = teclado.nextInt();
                }

            }while (true);
            teclado.nextLine();

            switch (opcao) {
                case 1 -> addAluno(universidade);
                case 2 -> addCurso(universidade);
                case 3 -> addNota(universidade);
                case 4 -> filterAluno(universidade);
                case 5 -> listAluno(universidade);
            }
        }
        }

        private static void menu() {
            System.out.println(" ");
            System.out.println("Esolha uma número das opções abaixo:");
            System.out.println("1 - Adicionar novo aluno.");
            System.out.println("2 - Adicionar curso a um aluno.");
            System.out.println("3 - Promover nota a aluno.");
            System.out.println("4 - Lista alunos de um curso.");
            System.out.println("5 - Listar todos os alunos");

        }
        private static void addAluno(Universidade universidade){
            System.out.println(" ");
            System.out.println("-----Adicionar Aluno-----");
            System.out.println(" ");

            System.out.print("Digite o nome do Aluno:");
            universidade.addAluno(teclado.next());


        }
        private static void addCurso(Universidade universidade){
            String nomeAluno;
            System.out.println(" ");
            System.out.println("-----Adicionar Curso a Aluno-----");
            System.out.println(" ");

            System.out.print("Digite o nome do Aluno:");
            nomeAluno = teclado.next();
            teclado.nextLine();

            for (int i = 0; i < universidade.getAlunos().length; i++) {
                if (universidade.getAlunos()[i].getNome().equals(nomeAluno)){
                    String nomeCurso;
                    System.out.print("Digite o nome do Curso:");
                    nomeCurso = teclado.nextLine();
                    universidade.getAlunos()[i].addCurso(nomeCurso);

                }
            }
        }
        private static void addNota(Universidade universidade){
            String nomeAluno;
            System.out.println(" ");
            System.out.println("-----Adicionar Nota a Aluno-----");
            System.out.println(" ");

            System.out.print("Digite o nome do Aluno:");
            nomeAluno = teclado.next();

            for (int i = 0; i < universidade.getAlunos().length; i++) {
                if (universidade.getAlunos()[i].getNome().equals(nomeAluno)){
                    double nota;
                    System.out.print("Digite o a nota:");
                    nota = teclado.nextDouble();
                    universidade.getAlunos()[i].setNota(nota);
                }
            }
        }
        private static void filterAluno(Universidade universidade){
            String nomeCurso;
            System.out.println(" ");
            System.out.println("-----Buscar alunos de um Curso-----");
            System.out.println(" ");
            System.out.print("Digite o nome do curso:");
            nomeCurso = teclado.nextLine();
            universidade.filter(nomeCurso);
        }
        private static void listAluno(Universidade universidade){
            System.out.println(" ");
            System.out.println("Abaixo esta a lista de alunos: ");
            universidade.listAlunos();
            System.out.println(" ");
        }
    }
#### Class Universidade
    import java.util.Arrays;

    public class Universidade {
        private Aluno[] alunos = new Aluno[1];

        public void addAluno(String nome){
            int index = alunos.length;
            Aluno aluno = new Aluno();
            aluno.setNome(nome);

            if (alunos[index - 1] == null){
                alunos[index - 1] = aluno;
            }else{
                alunos = aumentarArray(alunos, index);
                alunos[index] = aluno;
            }

            System.out.println(" ");
            System.out.printf("Aluno: %s adicionado", nome);
            System.out.println(" ");


        }

        public static Aluno[] aumentarArray(Aluno[] alunos, int index){
            Aluno[] newAlunos = new Aluno[index + 1];
            System.arraycopy(alunos, 0, newAlunos, 0, alunos.length);
            return newAlunos;
        }
        public static String[] aumentarArray(String[] alunosString, int index){
            String[] newAlunosString = new String[index + 1];
            System.arraycopy(alunosString, 0, newAlunosString, 0, alunosString.length);
            return newAlunosString;
        }

        public Aluno[] getAlunos() {
            return alunos;
        }

        public  void listAlunos(){
            for (Aluno aluno : alunos) {
                System.out.println(" ");
                System.out.printf("Nome: %s, Nota: %.2f, Cursos: %s", aluno.getNome(), aluno.getNota(), Arrays.toString(aluno.getCursos()));
                System.out.println(" ");
            }
        }

        public void filter(String curso){
            String[] alunosString = new String[1];
            int index = alunosString.length;

            for (Aluno aluno: alunos) {
                for (int i = 0; i < aluno.getCursos().length; i++) {
                    if (aluno.getCursos()[i].equals(curso)){
                        if (alunosString[index - 1] == null){
                            alunosString[index - 1] = aluno.getNome();
                        }else{
                            alunosString = aumentarArray(alunosString, index);
                            alunosString[index] = aluno.getNome();
                        }
                    }
                }

            }
            System.out.println(" ");
            System.out.printf("Os alunos matriculados em %s são:",curso );
            System.out.println(Arrays.toString(alunosString));
            System.out.println(" ");


        }
    }

#### Class Aluno
    public class Aluno {
    private String nome;
    private double nota;
    private String[] cursos = new String[1];

        public void addCurso(String curso){
            int index = cursos.length;

            if (cursos[index - 1] == null){
                cursos[index - 1] = curso;
            }else{
                cursos = aumentarArray(cursos, index);
                cursos[index] = curso;
            }
            System.out.println(" ");
            System.out.printf("Curso: %s. Foi adicionado ao aluno %s", curso, nome);
            System.out.println(" ");
        }

        public static String[] aumentarArray(String[] cursos, int index){
            String[] newCursos = new String[index + 1];
            System.arraycopy(cursos, 0, newCursos, 0, cursos.length);
            return newCursos;
        }


        public String getNome() {
            return nome;
        }

        public void setNome(String nome) {
            this.nome = nome;
        }

        public double getNota() {
            return nota;
        }

        public void setNota(double nota) {
            this.nota = nota;
            System.out.println(" ");
            System.out.printf("Nota: %.2f. Foi adicionado ao aluno %s", nota, nome);
            System.out.println(" ");
        }

        public String[] getCursos() {
            return cursos;
        }

    }
### Resultado da execução

O resultado da execução desse código foi demontrado no tópico de Funcionalidades, onde listamos todas as funções do nosso código e mostramos funcionando na prática.



### Diagrama de classes

![Diagrama de classes](https://github.com/iure06/Programming2-activitys/blob/main/LabWeek1/Questao3/src/images/Diagrama%20de%20classe%20quest%C3%A3o%203.png)

### Fluxograma

O fluxograma segue essa forma um pouco complexa pois o código executa em um loop infinito e só tem sua execução finalizada quando o usuário encerra o programa.

![Fluxograma](https://github.com/iure06/Programming2-activitys/blob/main/LabWeek1/Questao3/src/images/fluxograma3.png)

## Autores

- [@Ivanildo Iure L S](https://github.com/iure06?tab=repositories)


## Link do Projeto no GitHub


- [Questão 3](https://github.com/iure06/Programming2-activitys/tree/main/LabWeek1/Questao3)
