
# Formas Geométricas

Esse projeto aborda a resolução do problema da segunda questão do Lab Week da semana 1 do curso de Programming da Jala University.

Nesse desafio, tinhamos que estar crindo uma class abstrata de formas geométricas que tem como métodos o calculo da área e do périmetro. Esse projeto foi dividido em diversas classes distintas.




### Código
Abaixo tem todo o código de cada classe usada na resolução desse problema.

#### Class Main

    import Rectangle.Rectangle;
    import Rectangle.Square;
    import Triangulos.EquillateralTriangle;
    import Triangulos.Triangle;

    public class Main {
        public static void main(String[] args) {
            Circle circle = new Circle(5);
            circle.toStringR();

            Rectangle rectangle = new Rectangle(15,7);
            rectangle.toStringR();

            Triangle triangle = new EquillateralTriangle(7);
            triangle.toStringR();

            Rectangle square = new Square(2);
            square.toStringR();
        }
    }

#### Class FiguraGeometrica

    package Form;

    public abstract class FiguraGeometrica {

        public abstract void area();
        public abstract void perimetro();

        public abstract void toStringR();

    }

#### Class Circle

    import Form.FiguraGeometrica;

    public class Circle extends FiguraGeometrica {
        private double raio, area, perimetro;
        public Circle(double raio) {
            this.raio = raio;

            this.area();
            this.perimetro();
        }

        @Override
        public void area() {
            this.area = Math.PI * raio * raio;
        }

        @Override
        public void perimetro() {
            this.perimetro = 2 * Math.PI * raio;
        }

        @Override
        public void toStringR() {
            System.out.println(" ");
            System.out.printf("Círculo, área: %.2f perímetro: %.2f", this.area, this.perimetro);
            System.out.println(" ");
        }

    }

#### Class Rectangle

    package Rectangle;

    import Form.FiguraGeometrica;

    public class Rectangle extends FiguraGeometrica {
        protected double area;
        private double base,altura, perimetro;

        public Rectangle(double base, double altura) {
            this.base = base;
            this.altura = altura;

            this.area();
            this.perimetro();
        }

        @Override
        public void area() {
            this.area = base * altura;
        }

        @Override
        public void perimetro() {
            this.perimetro = 2 * (base + altura);
        }

        @Override
        public void toStringR() {
            System.out.println(" ");
            System.out.printf("Retângulo, área: %.2f perímetro: %.2f", this.area, this.perimetro);
            System.out.println(" ");
        }
    }


#### Class Square

    package Rectangle;

    public class Square extends Rectangle {
        private double comprimentoLado, perimetro;



        public Square(double comprimentoLado) {
            super(comprimentoLado,comprimentoLado );
            this.comprimentoLado = comprimentoLado;
            this.perimetro();
        }

        @Override
        public void perimetro() {
            this.perimetro = 4 * comprimentoLado;
        }

        @Override
        public void toStringR() {
            System.out.println(" ");
            System.out.printf("Quadrado, área: %.2f perímetro: %.2f", area, this.perimetro);
            System.out.println(" ");
        }
    }

#### Class Triangle

    package Triangulos;
    import Form.FiguraGeometrica;

    public class Triangle extends FiguraGeometrica {
        private double base, lado1, lado2;
        protected double area, perimetro;

        public Triangle(double base, double Lado1, double Lado2) {
            this.base = base;
            this.lado1 = Lado1;
            this.lado2 = Lado2;

            this.area();
            this.perimetro();
        }

        @Override
        public void area() {
            double semiperimmetro = (base + lado1 + lado2) / 2;
            // Usamos a fórmula de Heron para calcular a área
            this.area = Math.sqrt(semiperimmetro*(semiperimmetro - base)*(semiperimmetro - lado1)*(semiperimmetro - lado2));
        }

        @Override
        public void perimetro() {
            this.perimetro = base + lado1 + lado2 ;
        }

        @Override
        public void toStringR() {
            System.out.println(" ");
            System.out.printf("Triangulo, área: %.2f perímetro: %.2f", this.area, this.perimetro);
            System.out.println(" ");

        }
    }

#### Class EquillateralTriangle
    package Triangulos;
    
    public class EquillateralTriangle extends Triangle{
        public EquillateralTriangle(double base) {
            super(base, base, base);
    
        }
        @Override
        public void toStringR() {
            System.out.println(" ");
            System.out.printf("Triangulo Equilatero, área: %.2f perímetro: %.2f", area, perimetro);
            System.out.println(" ");
    
        }
    }


#### Class IsoscelesTriangle
    package Triangulos;
    
    public class IsoscelesTriangle extends Triangle{
        public IsoscelesTriangle(double base, double Lado1, double Lado2) {
            super(base, Lado1, Lado2);
        }
    
        @Override
        public void toStringR() {
            System.out.println(" ");
            System.out.printf("Triangulo Isósceles, área: %.2f perímetro: %.2f", area, perimetro);
            System.out.println(" ");
    
        }
    }


#### Class ScleneTriangle
    package Triangulos;
    
    public class ScleneTriangle extends Triangle{
        public ScleneTriangle(double base, double Lado1, double Lado2) {
            super(base, Lado1, Lado2);
        }
        @Override
        public void toStringR() {
            System.out.println(" ");
            System.out.printf("Triangulo Escaleno, área: %.2f perímetro: %.2f", area, perimetro);
            System.out.println(" ");
    
        }
    }

### Resultado da execução

![Imagem Terminal](https://github.com/iure06/Programming2-activitys/blob/main/LabWeek1/Questao2/src/images/formasGeometricas.png)



### Diagrama de classes

![Diagrama de classes](https://github.com/iure06/Programming2-activitys/blob/main/LabWeek1/Questao2/src/images/Diagrama%20de%20classe%20quest%C3%A3o%202.png)

### Fluxograma

![Fluxograma](https://github.com/iure06/Programming2-activitys/blob/main/LabWeek1/Questao2/src/images/fluxograma2.png)

## Autores

- [@Ivanildo Iure L S](https://github.com/iure06?tab=repositories)


## Link do Projeto no GitHub


- [Questão 2](https://github.com/iure06/Programming2-activitys/tree/main/LabWeek1/Questao2)
