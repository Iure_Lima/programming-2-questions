
# Seleção de quadrante e último dígito fatorial

Esse projeto aborda a resolução dos problemas I e II da primeira questão da Lab Week da semana 1 do curso de Programming da Jala University.




## Seleção de Quadrante

O primeiro desafio que abordaremos é o da seleção de quadrantre. O objetivo desse desafio é ler dois números x e y,levando em conta se seu valor é negativo ou positovo, estaremos imprimindo o quadrante.
### Código
No código abaixo, estou lendo um número x e fazendo as validações se  ele está no limite de (-1000 à 1000), se o número fornecido estiver dentro do limite o código segue sua execução. Em seguida estou lendo um valor de y e passando pela mesma validação.

Com os valores validados, fazemos um verificação se o valor de x é negativo ou positivo, depois do resultado da validação, vamos fazer a mesma validação com o valor de y.

Por fim, iremos imprimir na tela o quadrante dependendo dependendo de validação passada.

    import java.util.Scanner;

    public class SelecaoQuadrante {
        Scanner teclado = new Scanner(System.in);
        private int numberX, numberY;


        public SelecaoQuadrante() {
            System.out.print("Digite um número inteiro de x(-1000 à 1000): ");
            this.numberX = teclado.nextInt();
            do{
                if ( this.numberX >= -1000 && this.numberX <= 1000 && this.numberX != 0){
                    break;
                }else{
                    System.out.println("Número invalido! Tente novamente");
                    System.out.print("Digite um número inteiro de x(-1000 à 1000): ");
                    this.numberX = teclado.nextInt();
                }

            }while (true);

            System.out.print("Digite um número inteiro de y(-1000 à 1000): ");
            this.numberY = teclado.nextInt();
            do{
                if ( this.numberY >= -1000 && this.numberY <= 1000 && this.numberY != 0){
                    break;
                }else{
                    System.out.println("Número invalido! Tente novamente");
                    System.out.print("Digite um número inteiro de y(-1000 à 1000): ");
                    this.numberY = teclado.nextInt();
                }

            }while (true);
            quadrante();
        }

        public void quadrante(){
            if (this.numberX < 0){
                if (this.numberY < 0){
                    System.out.println("Quadrante 3");
                }else{
                    System.out.println("Quadrante 1");
                }
            }else{
                if (this.numberY < 0){
                    System.out.println("Quadrante 4");
                }else{
                    System.out.println("Quadrante 2");
                }
            }
        }

    }
### Resultado da execução

![Imagem Terminal](https://github.com/iure06/Programming2-activitys/blob/main/LabWeek1/Questao1/src/images/selecaoQuadrante.png)



### Fluxograma

![Fluxograma](https://github.com/iure06/Programming2-activitys/blob/main/LabWeek1/Questao1/src/images/Fluxograma11.png)

## Último Dígito Fatorial

O segundo desafio trata de calcular o valor de n números que serão fornecidos pelo usuário (esse números são os casos de prova). Depois de todos os números digitados, começamos a calcular o nosso fatorial de cada valor e imprimimos apenas o último dígito do resultado.

## Código

No código abaixo, temos uma validação para o número de casos, onde esse número não pode ser menor que 1 e nem maior que 10.Com o número  validado, pedimos que o usuário adicione um total de valores correspondete ao total de casos.

Para finalizar, calculamos o fatorial de cada valor dígitado anteriormente, convertemos esse valor em String e imprimimos o último dígito do valor resultante do fatorial.

    import java.util.Scanner;

    public class FatorialLD {
        Scanner teclado = new Scanner(System.in);
        private int quantidade;


        public FatorialLD() {
            System.out.print("Digite o número de casos de prova: ");
            this.quantidade = teclado.nextInt();

            do{
                if (this.quantidade > 1 && this.quantidade < 10){
                    break;
                }else{
                    System.out.println("Você digitou um número de casos invalidos");
                    System.out.print("Digite o número de casos de prova t(2 à 9): ");
                    this.quantidade = teclado.nextInt();
                }
            }while (true);

            int[] numbers = new int[this.quantidade];

            for (int i = 0; i < numbers.length; i++) {
                int numberCaso;
                System.out.print("Digite um número: ");
                numberCaso = teclado.nextInt();

                numbers[i] = numberCaso ;
            }

            this.ultimo_digits(numbers);

        }

        public void ultimo_digits(int[] numbers){
            int[] calcs = new int[this.quantidade];
            int calcIndex = 0;

            for (int number : numbers ) {
                int fatorial = 1;
                for (int i = 1; i <= number ; i++) {
                    fatorial *= i;
                }
                calcs[calcIndex] = fatorial;
                calcIndex += 1;
            }

            for (int calc: calcs ) {
                String calcString = String.valueOf(calc);
                int index = calcString.length();
                System.out.println(calcString.charAt(index - 1));
            }
        }
    }

### Resultado da execução
![Fluxograma](https://github.com/iure06/Programming2-activitys/blob/main/LabWeek1/Questao1/src/images/ultimoDigitoFatorial.png)
### Fluxograma
![Fluxograma](https://github.com/iure06/Programming2-activitys/blob/main/LabWeek1/Questao1/src/images/Fluxograma12.png)

## Autores

- [@Ivanildo Iure L S](https://github.com/iure06?tab=repositories)


## Link do Projeto no GitHub


- [Questão 1](https://github.com/iure06/Programming2-activitys/tree/main/LabWeek1/Questao1)
