import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner teclado = new Scanner(System.in);
        int opcao;

        System.out.println("Escolha uma das opições abaixo:");
        System.out.println("1 - Seleção de Quadrante");
        System.out.println("2 - Último digito fatorial");
        System.out.print("Sua escolha: ");
        opcao = teclado.nextInt();

        do {
            if (opcao > 0 && opcao <= 2){
                break;
            }else{
                System.out.println("Sua opição é invalida!");
                System.out.print("Qual a sua escolha: ");
                opcao = teclado.nextInt();
            }
        }while (true);

        if (opcao == 1){
            SelecaoQuadrante quadrante = new SelecaoQuadrante();
        }else{
            FatorialLD fatorial = new FatorialLD();
        }

    }
}